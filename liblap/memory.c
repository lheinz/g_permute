/*
 * This file contains some functions to simplify memory handling
 * of matrices and vectors. It was taken from   
 * the Jonker / Volgenant LAP code and adapted slightly
 * see http://www.magiclogic.com/assignment.html for details
 */

#include "gp_memory.h"

 void terminate(void)
{
  fprintf(stderr,"\n\nTerminated with fatal error.\n\n");
  exit(1);
}

 int *alloc_ints(int len)
{
        int *ptr;
        size_t  nobytes;

        if (len<1) len = 1;
        nobytes = (size_t) (len * sizeof(int));
        ptr = (int *)malloc(nobytes);

	return (ptr);
}

void free_ints(int *ints){
    free(ints);
}

 cost *alloc_costs(int len)
{
        cost *ptr;
        size_t  nobytes;

        if (len<1) len = 1;
        nobytes = (size_t) (len * sizeof(cost));
        ptr = (cost *)malloc(nobytes);

	return (ptr);
}

void free_costs(cost *costs){
    free(costs);
}


 cost   **alloc_costmatrix(int width, int height)
{
    cost **result = NULL;
    int i, j;

    if (width < 1) width = 1;
    if (height < 1) height = 1;

    result = (cost **)malloc(height * sizeof(cost *));
    if(result == NULL)
	return NULL;

    for(i=0; i<height; i++){
	result[i] = (cost *) malloc(width * sizeof(cost));

	if(result[i] == NULL){
	    for(j=0; j<i; j++)
		free(result[j]);
	    free(result);
	    return NULL;
	}
    }

    return result;
}

void free_costmatrix(cost **cmat, int width, int height){
    int i;

    for(i=0; i<height; i++)
	free(cmat[i]);
    
    free (cmat);
}
