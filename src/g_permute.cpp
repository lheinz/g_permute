/*
 * This file is part of the GROMACS molecular simulation package.
 *
 * Copyright (c) 1991-2000, University of Groningen, The Netherlands.
 * Copyright (c) 2001-2004, The GROMACS development team.
 * Copyright (c) 2011,2012,2013,2014,2015,2016,2017,2018, by the GROMACS development team, led by
 * Mark Abraham, David van der Spoel, Berk Hess, and Erik Lindahl,
 * and including many others, as listed in the AUTHORS file in the
 * top-level source directory and at http://www.gromacs.org.
 *
 * GROMACS is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License
 * as published by the Free Software Foundation; either version 2.1
 * of the License, or (at your option) any later version.
 *
 * GROMACS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GROMACS; if not, see
 * http://www.gnu.org/licenses, or write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA.
 *
 * If you want to redistribute modifications to GROMACS, please
 * consider that scientific software is very special. Version
 * control is crucial - bugs must be traceable. We will be happy to
 * consider code for inclusion in the official distribution, but
 * derived work must not be called official GROMACS. Details are found
 * in the README & COPYING files - if they are missing, get the
 * official version at http://www.gromacs.org.
 *
 * To help us fund GROMACS development, we humbly ask that you cite
 * the research papers on the package. Check out http://www.gromacs.org.
 */

/*  #define _VERBOSE  */

#include <string.h>
#include <math.h>
#include "gp_memory.h"
#include <lap.h>

#include "gromacs/commandline/pargs.h"
#include "gromacs/utility/fatalerror.h"
#include "gromacs/utility/futil.h"
#include "gromacs/utility/arraysize.h"
#include "gromacs/utility/cstringutil.h"
#include "gromacs/utility/smalloc.h"
#include "gromacs/trajectory/trajectoryframe.h"
#include "gromacs/fileio/trxio.h"
#include "gromacs/fileio/tpxio.h"
#include "gromacs/fileio/xtcio.h"
#include "gromacs/fileio/confio.h"
#include "gromacs/topology/topology.h"
#include "gromacs/topology/index.h"
#include "gromacs/math/vec.h"
#include "gromacs/pbcutil/pbc.h"
#include <omp.h>

/* pointers to two debug output files, only used in debug mode */
#ifdef __DEBUG
    static FILE *assignment, *costmatrix;
#endif

static bool bCom  = FALSE;
static bool bPBC  = TRUE;
static bool bOrp  = FALSE;
static bool bOref = FALSE;
static bool bRef  = FALSE;

/* permute - given a reference structure 'ref' *
 *           permute a structure 'now' containing 'natoms' *
 *           using the atoms with ids 'oxygens' to build up
 *           the costmatrix. Its 'result' contains the solution of the LAP *
 *           (i.e., the ids of the relabeled atoms) *
 * *
 *           'permute' is basically a wrapper for the 'lap' function *
 *           it is called by 'permute_trx' which performs all *
 *           administrative tasks s.a. reading / writing the files *
 *           and relabeling the atoms
 */

void permute(rvec *ref, rvec *now, int natoms, int* result, int *oxygens, t_pbc * pbc)
{
    int i, j;

    cost oldconf;  /* costs of the old (unpermuted) and new (permuted) */
    cost permconf; /* configurations */
    rvec dx;

    // allocate memory for the LAP variables */
    cost **assigncost, *u, *v, lapcost;
    row *colsol;
    col *rowsol;
    assigncost = (cost **) alloc_costmatrix(natoms, natoms);
    rowsol = (row *) alloc_ints(natoms);
    colsol = (col *) alloc_ints(natoms);
    u = (cost *) alloc_costs(natoms);
    v = (cost *) alloc_costs(natoms);

    if(assigncost == nullptr || rowsol == nullptr || colsol == nullptr
       || u == nullptr || v == nullptr)
    {
        /*	free_costmatrix(assigncost, natoms, natoms);not necessary, will be directly freed, when malloc failed*/
        free(rowsol);
        free(colsol);
        free(u);
        free(v);

        fprintf(stderr, "cannot allocate memory\n");
        exit(0);
    }

    /* compute the cost matrix */
    for (i = 0; i < natoms; i++)
    {
        for (j = 0; j < natoms; j++)
        {
            if (bPBC)
            {
                pbc_dx(pbc,ref[oxygens[i]],now[oxygens[j]], dx);
                assigncost[i][j] = norm2(dx);
            }
            else
                assigncost[i][j] = distance2(ref[oxygens[i]], now[oxygens[j]]);
        }
    #ifdef __DEBUG
        fwrite(assigncost[i], sizeof(cost), natoms, costmatrix);
    #endif
    };

    #ifdef __DEBUG
        fflush(costmatrix);
    #endif

    /*   start with clean vectors */
    for (i=0;i<natoms;i++)
    {
        result[i]=0;
        colsol[i]=0;
        u[i]=0;
        v[i]=0;
    };

    /* now solve the linear assignment problem to obtain the *
     * optimal permutation for this frame */
    lapcost = lap(natoms, assigncost, result, colsol, u, v);

    /* if we are debugging, dump the assignment to a file */
    #ifdef __DEBUG
        for(i=0; i<natoms; i++)
        {
            fprintf(assignment, "%d ", result[i]);
        }
        fprintf(assignment, "\n");
        fflush(assignment);
    #endif

    /* check the computed assignment for consistency */
    checklap(natoms, assigncost, result, colsol, u, v);

    /*   just to be sure, check whether we obtained a better configuration by *
     *   permuting the atoms */
    oldconf = permconf = 0;
    for(i=0; i<natoms; i++)
    {
        oldconf  += distance2(ref[oxygens[i]], now[oxygens[i]]);
        permconf += distance2(ref[oxygens[i]], now[oxygens[result[i]]]);
    }

    if(oldconf < permconf)
        printf("Warning: permutation does not reduce the distance\n");

    #ifdef __DEBUG
        /* allow debug file to be overwritten again */
        rewind(costmatrix);
    #endif

    // free allocated memory
    free_costmatrix(assigncost, natoms, natoms);
    free_ints(rowsol);
    free_ints(colsol);
    free_costs(u);
    free_costs(v);

    return;
}

/*builds center or mass coordinates if no reference structure was given */
void  BuildxComAr(int lnRes, t_topology ltop, t_trxframe lframe, int *lnAtomsRes, int **lresIndex, rvec *lxCom)
{
    int ix,i,j;
    rvec x_tmp2;
    real M,mass;

    /* Get center of mass of residues */
    for (i = 0; i < lnRes; i++)
    {
        M=0;
        clear_rvec(lxCom[i]);
        for (j = 0; j < lnAtomsRes[i]; j++)
        {
            ix = lresIndex[i][j];
            mass  = ltop.atoms.atom[ix].m;
            M += mass;
            svmul(mass, lframe.x[ix], x_tmp2);
            rvec_inc(lxCom[i],x_tmp2);
        }
        for(j=0; (j<DIM); j++)
        {
            lxCom[i][j] /= M;
        }
    }
}

/*initialise frame */
/*PURPOSE: when in COM mode and no reference struct is supplied*/
t_trxframe initFrame(int step, real time, t_atoms *atoms, rvec *vec, matrix box, int natoms)
{
    // struct defined in trajectoryframes.h; still needs ePBC and bPBC, *index, ...
    t_trxframe comframe;

    clear_trxframe(&comframe, TRUE);
    // comframe.flags = 1; // no longer needed
    comframe.natoms = natoms;
    comframe.bStep = TRUE;
    comframe.step = step;
    comframe.bTime = TRUE;
    comframe.time = time;
    comframe.bAtoms = atoms != nullptr;
    comframe.atoms = atoms;
    comframe.bX = TRUE;
    comframe.x = vec;
    comframe.bV = FALSE;
    comframe.v = nullptr;
    comframe.bBox = TRUE;
    copy_mat(box,comframe.box);

    return comframe;
}

/* Copy trxframe frame and box info from source to dest */
void copy_trxframe_stuff(t_trxframe * src, t_trxframe * dest)
{
    dest->time = src->time;
    copy_mat(src->box, dest->box);
    dest->step = src->step;
    dest->prec = src->prec;
}

/*print frame info up to no atoms of specified frame */
/*PURPOSE: debugging  - mainly*/
void printFrame(t_trxframe comframe, int no)
{
    int i,j;

    printf("printing frame:\n step %ld time %f\n",comframe.step, comframe.time);
    printf(" natoms %d\n",comframe.natoms);

    for (i=0;i<no;i++)
    {
        printf("\ni %d frame %f %f %f\n",i,comframe.x[i][0],comframe.x[i][1],comframe.x[i][2]);fflush(stdout);

    }
    printf("\n printing box vectors\n");

        for (j=0;j<3;j++)
        {
            printf("XX  %f",comframe.box[XX][j]);
            printf("YY  %f",comframe.box[YY][j]);
            printf("ZZ  %f",comframe.box[ZZ][j]);
        }
        printf("\n");
}


/* permute_trx - given the filenames of 'infile', 'indexfile'                       *
 *               and 'outfile' and 'reffile' (if any) and the 'molsize'             *
 *               of the molecules to be relabeled, read all the files               *
 *               and solve the LAP for each frame of 'infile'.                      *
 *               The permuted trajectory is written to 'outfile'                    *
 *               -orp on command line triggers output of CenterOfMass coords to              *
 *               refperm.xtc and -oref will write COM coords of original trajectory *
 *               to ref.xtc                                                         */

void permute_trx(const char *infile, const char *indexfile, const char* outfile, const char *reffile, const char *resid_file,
				 const char *outfile_ref, const char *out_refperm_file, int molsize, gmx_output_env_t *oenv)
{
    int             ftp, ftpndx; /* flags holding the file types of infile and indexfile*/
    t_trxstatus    *trxhandle;   /* file handle for infile*/   // formerly int
    t_trxstatus    *trxout;      /* file handle for outfile*/  // formerly int

    int             natoms;
    int             i = 0, j = 0, res = 0;    /*loop variables*/
    int		        nRes = 0;
    t_topology      top;
	int            *nAtomsRes = nullptr;
	int           **resIndex = nullptr;
    int             ndist, nref, nsol; /* number of atoms for distance matrix *
			                            * calculation, reference structure and solvent group */
    bOref = outfile_ref == nullptr ? 0 : 1;
    bOrp = out_refperm_file == nullptr ? 0 : 1;
    t_trxframe      frame;
    rvec           *refstruct, *xref;
    t_topology      ref_top;
    int             ePBC = -1;

    int            *dist, *sol;             /* atom ids for distance calculation and solvent group */

    char           *distname, *solname, str[STRLEN];

    char            title[STRLEN];
    int             resnr, lastRes = -100, nThisRes = 0, iRes, ix, nAtomsPerResMax = 0;
    t_trxstatus    *trxout_ref = nullptr, *trxout_refPerm = nullptr, *statusCom, *statusComPerm;
    int             d = 0, m = 0;
    rvec           *x_tmp = nullptr;
    matrix          box;
    int            *zeroToNResArray = nullptr;
    rvec           *xCom = nullptr, x_tmp2, *xcomRes;
    real            tCom;
    rvec           *v;
    t_atoms        *atoms, *comAtoms = nullptr;

    ftp = fn2ftp(infile);
    ftpndx = fn2ftp(indexfile);

    // parallel environment stuff
    int             max_blocksize = 800;  // process this many frames per block
    int             blocksize = max_blocksize;
    omp_set_num_threads(omp_get_max_threads());
    t_trxframe     *frames;
    t_trxframe     *comFrames;
    t_trxframe     *comFramesPerm;
    snew(frames, max_blocksize);
    snew(comFrames, max_blocksize);
    snew(comFramesPerm, max_blocksize);
    int            *alls;  // store permuted atom indices here; flatten list for parallelization


    /* let the user choose the distance and the solvent group */
    /* see g_permute user documentation for details */
    printf("\nChoose a group for the distance calculation:\n");
    rd_index(indexfile, 1, &ndist, &dist, &distname);
    printf("\nChoose the solvent group:\n");
    rd_index(indexfile, 1, &nsol, &sol, &solname);

    #ifdef __VERBOSE
        printf("read index: ndist = %d\n", ndist);
        for(i=0; i<ndist; i++)
        {
            printf("%d ", dist[i]);
        }
        printf ("\n");
    #endif

    /* open the output file */
    printf ("writing trajectory to %s\n", outfile);
    trxout = open_trx(outfile, "w");

    /* initialize the vector containing the permuted positions */

    /* open the trajectory and read the first frame */
    if (!read_first_frame(oenv, &trxhandle, infile, &frame, TRX_READ_X || TRX_READ_V || TRX_READ_F))
        gmx_fatal(FARGS,"Could not read first frame from trajectory %s",infile);

    natoms = frame.natoms;

    snew(atoms,natoms);
    snew(v, natoms);
    snew(refstruct,natoms);    /*   allocate space for the reference structure */

    /*read in tpr if (bCom)*/
    if (bCom){
        read_tps_conf(resid_file, &top, &ePBC, &x_tmp, nullptr, box, TRUE); /*must have*/
	    for (i = 0; i < ndist; i++)
	        if ( (resnr = top.atoms.atom[dist[i]].resind) != lastRes)
            {
		        lastRes = resnr;
	      	    nRes++;
		        if (nThisRes > nAtomsPerResMax)
		            nAtomsPerResMax = nThisRes;
                nThisRes=1;
	        }
	        else
	        {
		        nThisRes++;
            }
        if (!bRef)
        {
            snew(refstruct,nRes);
        }
        snew(comAtoms,nRes);

        init_t_atoms(comAtoms,nRes,FALSE);
        snew(nAtomsRes,nRes);
        snew(xCom,nRes);
        snew(zeroToNResArray,nRes);
        snew(resIndex,nRes);
        for (i = 0; i < nRes; i++)
        {
            snew(resIndex[i],nAtomsPerResMax);
            zeroToNResArray[i] = i;
        }
        lastRes = -100;
        iRes = -1;
        nAtomsRes[iRes] = 0;
	    j=0;
	    for ( i = 0; i < ndist; i++ )
	    {
            if ( (resnr=top.atoms.atom[dist[i]].resind) != lastRes)
            {
                if (iRes >= 0)
                {
                    nAtomsRes[iRes] = j+1;
                }
                iRes++;
                lastRes = resnr;
                j = 0;
            }
            else
            {
                j++;
            }
            resIndex[iRes][j] = dist[i];
        }
        j++;
        nAtomsRes[nRes-1] = j;
    } /*if bCom end*/


    /*     if possible, load the reference structure from a data file *
     *     if no -r is given use structure from first frame           *
     *     and fill it into refstruct                                 */

	if(bRef)
    {
        // TODO: this should be optimized
        read_tps_conf(reffile, &ref_top, &ePBC, &refstruct, &v, box, FALSE);
        t_atoms* atoms_tmp;
        snew(atoms_tmp, 1);
        *atoms_tmp = ref_top.atoms;
        atoms->nr = atoms_tmp->nr;
        sfree(atoms_tmp);

		/* create some dummy variables to store excess information */
		//get_stx_coordnum(reffile, &(atoms->nr));
		nref = atoms->nr;
		init_t_atoms(atoms, atoms->nr, TRUE);
		if (nref != natoms && bCom == FALSE)
        {
		  gmx_fatal(FARGS,
				"found %d atoms in the trajectory frames, "
				"but %d atoms in reference structure", natoms, nref);
		} else if (nref != nRes && bCom == TRUE)
		{
            /*bCom on and different no of COM coords in reffile then residues in structure file*/
              gmx_fatal(FARGS,
                "found %d residues in the structure file, "
                "but %d COM coordinates in reference structure", nRes, nref);
        }
		// read_stx_conf(reffile, title, atoms, refstruct, v, box); // original version

		sfree(v);
		sfree(atoms);
	} else
	{
	/*     the user did not supply a reference structure */
	/*     use the first frame instead */
        if (!bCom)
        {
            for ( i = 0; i < natoms; i++ )
            {
            copy_rvec(frame.x[i], refstruct[i]);
            }
        } else
        {
            printf ("\nNo reference structure provided, building COM coordinates from first frame\n");
            /* we built the com coords for each residue and save it in refstruct*/
            /* in frame are already the coords we need*/
            BuildxComAr(nRes, top, frame,nAtomsRes,resIndex,refstruct);
        }
    } /*end of if reffile*/

    if (bCom)
    {
        for (int thread = 0; thread < max_blocksize; thread++)
        {
            if (bRef)
            {
                if (bOref)
                    read_first_frame(oenv, &statusCom,     reffile, &(comFrames[thread]),     0);
                if (bOrp)
                    read_first_frame(oenv, &statusComPerm, reffile, &(comFramesPerm[thread]), 0);
            } else
            {
                 if (bOref)
                 {
                    comFrames[thread] = initFrame(0,0,comAtoms,xCom,box,nRes);
                 }
                 if (bOrp)
                 {
                     comFramesPerm[thread] = initFrame(0,0,comAtoms,xCom,box,nRes);
                 }
            }
        }

        if (bOref)
        {
            printf ("\nwriting trajectory of centers of mass to %s\n", outfile_ref);
            trxout_ref = open_trx(outfile_ref, "w");
        }
        if (bOrp)
        {
            printf ("\nwriting trajectory of permuted centers of mass to %s\n", out_refperm_file);
            trxout_refPerm = open_trx(out_refperm_file, "w");
        }
    } else
    {
        if (bOref)
        {
            printf("\nOutput to %s only when -com is specified\n\n", outfile_ref);
        }
        if (bOrp)
        {
            printf("\nOutput to %s only when -com is specified\n\n", out_refperm_file);
        }
    }/*end of if bCom*/

    /*read all the following structures permute them and dump them in the *
    * output file *
    * Note that the first structure is skipped !*/

    // initialize frames with first frame of trajectory
    for (int thread = 0; thread < max_blocksize; thread++)
    {
        if ( !read_first_frame(oenv, &trxhandle, infile, &(frames[thread]), TRX_READ_X || TRX_READ_V || TRX_READ_F) )
            gmx_fatal(FARGS,"Could not read first frame from trajectory %s",infile);
    }
    snew(alls, natoms*max_blocksize);

    while (true)
    {
        // read and write frames in single thread, process them in parallel
        for (int thread = 0; thread < max_blocksize; thread++)
        {
            // check if we reached end of traj; if yes, limit further for-loops to blocksize and
            // stop reading frames.
            if( !read_next_frame(oenv, trxhandle, &(frames[thread])) )
            {
                blocksize = thread;
                break;
            }
        }

        // do the actual work here
        #pragma omp parallel for private(i, j, res, d, m) schedule(static, 1)
        for (int thread = 0; thread < blocksize; thread++)
        {
            // initialize private variables
            /* these variables containing the cost matrix and the solution of the LAP. */
            rvec hbox;
            int *result = nullptr; /* atom ids returned from the LAP solver (i.e., only ndist atoms) */
            rvec *xCom = nullptr;
            t_pbc pbc;
            set_pbc(&pbc, ePBC, frames[thread].box);
            if (omp_get_thread_num() == 0)
                fprintf(stderr, "Time %6.3f ps\n", frames[thread].time);

            /* call the LAP solver */
            if (!bCom)
            {
                snew(result,ndist);
                permute(refstruct, frames[thread].x, ndist, result, dist, &pbc);
            }
            else
            {
                snew(result,nRes);
                snew(xCom,nRes);
                BuildxComAr(nRes, top, frames[thread], nAtomsRes, resIndex, xCom);
                if (bOref)
                {
                    /* Write the positions of the center of mass of each residue */
                    for (i=0; i<nRes; i++)
                    {
                        copy_rvec(xCom[i], comFrames[thread].x[i]);
                    }

                    copy_trxframe_stuff(&(frames[thread]), &(comFrames[thread]));
                }
                /* Now, permute uses all positions in xCom, therefore
                   zeroToNResArray contains only 0,1,2,...,nRes-1. */
                permute(refstruct, xCom, nRes, result, zeroToNResArray, &pbc);
            }/*end of if (!bCom)*/

            /* now relabel the atoms in the current frame according to  */
            /* the optimal permutation 'result' */

            /* start from the identity permutation */
            for(i=0; i<natoms; i++)
            {
                alls[i+thread*natoms] = i;
            }
            /*flip the positions of all solvent molecules according to 'result'*
             * the for loop loops through all solvent atoms  *
             * j stores the current molecules' index *
             * res loops through the atoms in a molecule *
             * so that each solvent molecules is relabeled as a whole */
            for (i=0;i<nsol;i++)
            {
                res = i / molsize;
                j = i % molsize;
                alls[sol[i]+thread*natoms] = sol[molsize*result[res]+j];
            }
            /* If the COM is more that half a boxlength away from the reference position
               shift the whole molecule (and its center of mass)                         */
            if (bCom)
            {
                for( d = 0; d < DIM; d++ )
                    hbox[d] = 0.5*frames[thread].box[d][d];
                for ( i = 0; i < nRes; i++ )
                {
                    for( m = DIM-1; m >= 0; m-- )
                        if (hbox[m] > 0)
                        {
                            while ( xCom[result[i]][m] - refstruct[i][m] <= -hbox[m] )
                                for( d = 0; d <= m; d++ )
                                {
                                    for ( j = 0; j < molsize; j++ )
                                        frames[thread].x[molsize*result[i]+j][d] += frames[thread].box[m][d];
                                    xCom[result[i]][d]              += frames[thread].box[m][d];
                                }
                            while ( xCom[result[i]][m]-refstruct[i][m] > hbox[m] )
                                for( d = 0; d <= m; d++ )
                                {
                                    for ( j = 0; j < molsize; j++ )
                                        frames[thread].x[molsize*result[i]+j][d] -= frames[thread].box[m][d];
                                    xCom[result[i]][d]              -= frames[thread].box[m][d];
                                }
                        }
                }
                if (bOrp)
                {
                    for ( i = 0; i < nRes; i++ )
                    {
                        copy_rvec(xCom[result[i]], comFramesPerm[thread].x[i]);
                    }
                    copy_trxframe_stuff(&(frames[thread]), &(comFramesPerm[thread]));
                }
            }
            sfree(result);
        }

        // write frames here
        for (int thread = 0; thread < blocksize; thread++)
        {
            write_trxframe_indexed(trxout, &(frames[thread]), natoms, &(alls[thread*natoms]), nullptr); // not writing to PDB, so setting gmx_connect to NULL
            if (bCom)
            {
                if (bOref)
                {
                    write_trxframe(trxout_ref, &(comFrames[thread]), nullptr);  // not writing to PDB, so setting gmx_connect to NULL
                }
                if (bOrp)
                {
                    write_trxframe(trxout_refPerm, &(comFramesPerm[thread]), nullptr); // not writing to PDB, so setting gmx_connect to NULL
                }
            }
        }

        // break if we have reached the end of the trajectory
        if (blocksize < max_blocksize)
        {
            break;
        }
    }

    sfree(frames);
    sfree(comFrames);
    sfree(comFramesPerm);
    sfree(alls);

    close_trx(trxout);
    if (bCom)
    {
        if (bOrp)
            close_trx(trxout_refPerm);
        if (bOref)
            close_trx(trxout_ref);
/*			for (i=0;i<nRes;i++)
          sfree(refstruct[i]);*/
    }
}

int main( int argc, char *argv[])
{
    const char *desc[] =
    {
        "[THISMODULE] reads ",
        "a trajectory ([TT].trj[tt]/[TT].trr[tt]/[TT].xtc[tt]) ",
        "output in a readable format and fits all frames against the ",
        "structure file using combinatorial optimization.[PAR]"
    };
    t_filenm fnm[] =
    {
        { efTRX, "-f",     nullptr,       ffREAD  },
        { efTRX, "-o",    "permute.xtc",  ffWRITE },
        { efTRX, "-oref", "ref.xtc",      ffOPTWR },
        { efTRX, "-orp",  "refperm.xtc",  ffOPTWR },
        { efNDX, "-n",    "index.ndx",    ffREAD  },
        { efSTX, "-r",    "ref.gro",      ffOPTRD },
        { efSTX, "-s",    "frame.pdb",    ffOPTRD }
    };

    #define NFILE asize(fnm)

    /* Command line options */
    static int molsize = 3;
    const char *out_file;
    const char *in_file;
    const char *index_file;
    const char *ref_file;
    const char *out_refperm_file;
    const char *resid_file;
    const char *out_file_ref;
    int ftp,ftpin, ftpndx;
    bool bOref,bOrp;
    t_pargs pargs[] =
    {
        { "-m", FALSE, etINT, {&molsize},
            "number of atoms of a solvent molecule" },
        { "-com", FALSE, etBOOL, {&bCom},
            "consider center of mass of each residue as reference point for distance calculation."
            "if desired give appropriate reference file with -r, otherwise reference structure is built from COM of residues" },
        { "-rm_pbc", FALSE, etBOOL, {&bPBC},
            "calc distance using pbc, default is to remove pbc" },
    };

    //CopyRight(stderr,argv[0]); -> replace with something?

    gmx_output_env_t *oenv; // consider passing this as argument in the future
    if (!parse_common_args(&argc, argv, PCA_CAN_TIME, NFILE, fnm, asize(pargs), pargs,
            asize(desc), desc, 0, nullptr, &oenv))
    {
        return 0;
    }

    /* open files for debug output (mainly crash forensics)*
    *  assignment - contains the latest assignment *
    *  costmatrix - contains the last cost matrix  */
    #ifdef __DEBUG
    assignment = fopen("assign.dat", "w");
    costmatrix = fopen("matrix.dat", "w");
    #endif /* _DEBUG*/

    in_file          = opt2fn("-f", NFILE, fnm);
    index_file       = opt2fn("-n", NFILE, fnm);
    out_file         = opt2fn("-o", NFILE, fnm);
    out_file_ref     = opt2fn("-oref", NFILE, fnm);
    ref_file         = opt2fn("-r", NFILE, fnm);
    out_refperm_file = opt2fn("-orp", NFILE, fnm);
    resid_file       = opt2fn("-s", NFILE, fnm);

    bOref            = opt2bSet("-oref", NFILE, fnm);
    bOrp             = opt2bSet("-orp", NFILE, fnm);
    bRef             = opt2bSet("-r", NFILE, fnm);

    if (!gmx_fexist(ref_file) && (bRef))
    {
        gmx_fatal(FARGS,"Reference structure %s not found",ref_file);
    } else if (!bRef)
    {
        ref_file = nullptr;
    }

    out_file_ref = bOref == 1 ? out_file_ref:nullptr; /* making sure NULL is set if none of     *
                                                  * the two options -oref / -orp are given */
    out_refperm_file = bOrp == 1 ? out_refperm_file:nullptr;

    /* Determine output type */
    ftp = fn2ftp(out_file);
    fprintf( stderr, "Will write %s: %s\n", ftp2ext(ftp), ftp2desc(ftp) );

    if (bCom && (!(resid_file)))
      gmx_fatal(FARGS, "\n\nYou must supply  a structure (-s)\nThe structure file (-s) is used to create the molecule groups using the residue id.\nYou also can provide a reference file, which must contain one atom per molecule you want to permute. Using the first structure is the default if -r is not provided a.\n\n");

    /* all IO has been set up: now we do your job and permute the trajectory */
    if (ftp2bSet(efTRX,NFILE,fnm))
        permute_trx(in_file, index_file, out_file, ref_file, resid_file, out_file_ref, out_refperm_file, molsize, oenv);

    //thanx(stderr); // rename to gmx_thanx or leave out completely

    return 0;
}
